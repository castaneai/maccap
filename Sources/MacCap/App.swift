import Foundation
import ScreenCaptureKit

@main
struct MacCap {
    static func main() async throws {
        let recorder = ScreenRecorder()
        let display = try await getFirstDisplay()
        var saved = false
        try await recorder.start(display: display) { buffer in
            if saved {
                return
            }
            
            let flags = CVPixelBufferLockFlags(rawValue: 0)
            CVPixelBufferLockBaseAddress(buffer, flags)
            defer {
                CVPixelBufferUnlockBaseAddress(buffer, flags)
            }
            
            let width = CVPixelBufferGetWidth(buffer)
            let height = CVPixelBufferGetHeight(buffer)
            
            let image = CIImage(cvPixelBuffer: buffer)
            let colorSpace = image.colorSpace ?? CGColorSpaceCreateDeviceRGB()
            log("Captured frame (size: \(width) x \(height), colorSpace: \(colorSpace)")
            let context = CIContext()
            guard let data = context.pngRepresentation(of: image, format: .RGBA8, colorSpace: colorSpace) else {
                log("failed to convert pixel buffer to PNG")
                return
            }
            FileHandle.standardOutput.write(data)
            saved = true
        }
        sleep(1)
    }
}

func getFirstDisplay() async throws -> SCDisplay {
    let sc = try await SCShareableContent.excludingDesktopWindows(false, onScreenWindowsOnly: true)
    return sc.displays.first!
}
