import Foundation

var stderr = FileHandle.standardError

func log(_ msg: String) {
    print(msg, to: &stderr)
}

extension FileHandle: TextOutputStream {
    public func write(_ string: String) {
        guard let data = string.data(using: .utf8) else {
          return
        }
        try? write(contentsOf: data)
    }
}
