import Foundation
import ScreenCaptureKit

class ScreenRecorder: NSObject {
    
    private var onFrame: ((CVPixelBuffer) -> Void)?
    private let frameOutputQueue = DispatchQueue(label: "frame-handling")
    
    func start(display: SCDisplay, onFrame: @escaping (CVPixelBuffer) -> Void) async throws {
        self.onFrame = onFrame
        let filter = SCContentFilter(display: display, excludingWindows: [])
        let streamConfig = SCStreamConfiguration()
        streamConfig.pixelFormat = kCVPixelFormatType_32BGRA
        streamConfig.width = Int(display.width) * 2
        streamConfig.height = Int(display.height) * 2
        streamConfig.minimumFrameInterval = CMTime(value: 1, timescale: CMTimeScale(60))
        streamConfig.queueDepth = 5
        let stream = SCStream(filter: filter, configuration: streamConfig, delegate: self)
        try stream.addStreamOutput(self, type: .screen, sampleHandlerQueue: frameOutputQueue)
        try await stream.startCapture()
    }
}

extension ScreenRecorder: SCStreamOutput {
    func stream(_ stream: SCStream, didOutputSampleBuffer sampleBuffer: CMSampleBuffer, of type: SCStreamOutputType) {
        guard sampleBuffer.isValid else {
            log("Invalid sample buffer found, ignored")
            return
        }
        guard let attachmentsArray = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, createIfNecessary: true) as? [[SCStreamFrameInfo: Any]],
              let attachments = attachmentsArray.first else {
            log("failed to retrive the attachments from the sample buffer")
            return
        }
        guard let statusRawValue = attachments[SCStreamFrameInfo.status] as? Int,
              let status = SCFrameStatus(rawValue: statusRawValue) else {
            log("Failed to get the frame status from the attachments.")
            return
        }
        guard status == .complete else {
            log("Skip updating the frame because the frame status is \(String(describing: status))")
            return
        }
        guard let pixelBuffer = sampleBuffer.imageBuffer else {
            log("Failed to get a pixel buffer from the sample buffer.")
            return
        }
        guard let onFrame = onFrame else {
            return
        }
        onFrame(pixelBuffer)
    }
}

extension ScreenRecorder: SCStreamDelegate {
    func stream(_ stream: SCStream, didStopWithError error: Error) {
        log("Stream stopped with error: \(error)")
    }
}
