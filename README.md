# MacCap

Capture the current screen as a PNG image (RGBA8 format).

```sh
swift run > test.png
```
